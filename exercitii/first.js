//////1.a
let vec = [-2,-3,8,-7]
let c=0;
for(let i=0;i<vec.length;i++)
{
    if(vec[i]<0) c++;
}

if(c%2==0) console.log("PLUS")
else console.log("MINUS");

////1.b

let weight = 3000;

switch (weight){
    case 10: console.log("pen");
        break;
    case 200: console.log("cat");
        break;
    case 3000:console.log("dog");
        break;
    case 40000: console.log("horse");
        break;
    default: console.log("A truckload of bunnies");

}

///2.a

let sumN = function (n){
    let sum=0;
    for(let i=0;i<n;i++)
        sum+=i;
    return sum;
}

console.log("\n"+sumN(10));

//2.b

let x=30;

function lessThan10k(x){
    while (x<10000){
        x=x*Math.random()*10;
        console.log(x);
    }
}

console.log(lessThan10k(x));

//3.a

let arr=[];
let toAdd=[89, 99, 120, 412, 124];
for(let i=0;i<50;i++){
    arr[i]=i;
}
for(let i=0;i<toAdd.length;i++) {
    arr.push(toAdd[i]);
}
console.log(arr);

//3.b
console.log(arr.pop());
console.log(arr);

///4
let obj={
    age: 20,
    firstName: "gigel",
    lastName: "zzz"
}

console.log(obj);

let obj2 = {};
obj2.info=obj.age+" "+obj.firstName+" "+obj.lastName;

console.log(obj2.info);

//5.b

arr.forEach((element)=>{ element%3!==0? console.log(element):null;});


//6
class human{
    constructor(name) {
        this.name = name;
    }

    get name(){
        return this._name;
    }

    set name(value){
        value = value.charAt(0).toUpperCase() + value.slice(1);
        this._nume=value;
    }
    whoYouAre(){
        return "I'M "+this._nume;
    }
}

let om1= new human("vasile");
console.log(om1.whoYouAre());