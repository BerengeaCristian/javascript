//  Part I
//  1. Get the main content container
document.getElementById('main');
//  2. Get first post title
document.querySelector( '.post .entry-title').innerText;

//  3. Get first post content
document.querySelector('.post');
//  4. Get all post titles
let arr=document.querySelectorAll('.post .entry-title');
arr.forEach(element => console.log(element.innerText ));
//  5. Change the value for the first title
document.querySelector( '.post .entry-title').innerText='zzzz';
//  6. Change the URL for the first title link
document.querySelector( '.post .entry-title a').href='https://www.google.com/';
//  7. Change the background color for the body
document.body.style.backgroundColor = '#aaa';
//  8. Add a new class to the articles then add styles
let elements = document.getElementsByTagName('article');
for(let i=0;i<elements.length;i++){
    elements[i].classList.add('mystyle');
}


//  Part II
//  1.  Select the parent element for the first post title
document.querySelector( '.post .entry-title').parentElement;
//  2.  Select the first post and log the sibligns
let post1 = document.querySelector( '.post ');
while(post1.nextSibling!==null){
    console.log(post1);
    post1=post1.nextSibling;
}

//  3.  Select the #main container and log the children
let childrens = document.getElementById('main').children;
for(let i=0;i<childrens.length;i++)
{
    console.log(childrens[i]);
}

// Bonus Set the entry content with the curent date and time and update it every x (as parameter) seconds